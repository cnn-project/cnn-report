%!TEX program = xelatex
%!TEX root = report.tex
%!TEX options = --shell-escape
%!TEX output_directory = out
%!TEX spellcheck = fr-FR
% !BIB program = biblatex 

\documentclass[DIV=15, openany, 9pt]{scrreprt}
\renewcommand\sc{\textsc}
% core
\usepackage{fontspec}
\usepackage[french]{babel}
\usepackage[headsepline, footsepline]{scrlayer-scrpage}

% glossary & references
\usepackage[nonumberlist, numberedsection, acronym, shortcuts]{glossaries}
\makeglossaries

% science
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{amsthm}
\usepackage{subcaption}
\usepackage{import}

% media
\usepackage[svgnames]{xcolor}
\usepackage{tikz}
\usetikzlibrary{shapes, calc, patterns}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage[outputdir=out]{minted}
\usepackage{hyperref}
\hypersetup{
    colorlinks,
    breaklinks,
    linkcolor=DarkSlateBlue,
    citecolor=DarkOrchid,
    urlcolor=DarkSlateBlue
}

% fonts selection
\usepackage[default, regular, semibold]{sourceserifpro}
\usepackage[regular, semibold]{sourcesanspro}
\usepackage[regular, semibold, ligatures=TeX]{sourcecodepro}

\setminted{
    breaklines,
    fontsize=\scriptsize,
    framesep=2mm,
    mathescape,
    autogobble,
    frame=none,
    rulecolor=black,
    style=friendly,
    bgcolor=white,
    tabsize=2
}

\subject{UV Projet Mobile image}
\title{Rapport de projet mobile image}
\subtitle{Application mobile avec une API Django REST combinée à un réseau de neurones à convolution permettant l'analyse d'images}
\author{Benjamin Boboul \and Corentin Le Berre}
\date{\today}
\publishers{IMT Lille-Douai}

\begin{document}


    \maketitle

    \tableofcontents

    \chapter{Guide utilisateur}

    Ce chapitre présente le guide d'utilisation de l'application, nous allons voir son interface ainsi que la manière de rechercher sa propre image.

    \section{Découverte de l'interface}

    Pour le développement de la partie cliente, nous avons décidé d’utiliser le Framework Flutter via le langage Dart. Dans flutter, les widgets sont les éléments de base de l'interface utilisateur d'une application Flutter. Chaque widget est une déclaration immuable d'une partie de l'interface utilisateur.  \\

    Ce Framework a l’intérêt d’être multiplateformes (iOS/Android/Web/Desktop) tout en s’adaptant de manière naturelle au style de chaque plateforme.  De plus, il est rapide et propose des fonctionnalités avancées comme un ‘package manager’ ainsi que le ‘hot reload’ et des performances comparables à des applications natives. \\

    Notre application a été développée et testée dans l'objectif d'être supportée par les plateformes iOS et Android. Les captures présentées vous présentent le parcours type d’un utilisateur, elles ont été réalisées sur un émulateur iOS.

    \begin{figure}[ht]
        \centering
        \begin{subfigure}{.15\linewidth}
            \includegraphics[width=\linewidth]{images/home.png}
        \end{subfigure}
        \begin{subfigure}{.15\linewidth}
            \includegraphics[width=\linewidth]{images/home-2.png}
        \end{subfigure}
        \begin{subfigure}{.15\linewidth}
            \includegraphics[width=\linewidth]{images/send.png}
        \end{subfigure}
        \begin{subfigure}{.15\linewidth}
            \includegraphics[width=\linewidth]{images/crop.png}
        \end{subfigure}
        \begin{subfigure}{.15\linewidth}
            \includegraphics[width=\linewidth]{images/list-2.png}
        \end{subfigure}
        \begin{subfigure}{.15\linewidth}
            \includegraphics[width=\linewidth]{images/list.png}
        \end{subfigure}
   
        \caption{Captures d'écran de l'application}
    \end{figure}


    \begin{wrapfigure}{r}{0.15\linewidth}
        \centering
        \includegraphics[width=\linewidth]{images/final.png}
    \end{wrapfigure}

    Lors de sa première exécution, l'application affiche un simple texte invitant l'utilisateur à analyser une image. Cet écran de démarrage est illustré par la première capture que nous avons ci-dessus. Une fois que l'utilisateur appuie sur le bouton, l'application propose alors de choisir entre prendre une photo avec l'appareil du terminal ou bien de sélectionner une image depuis la galerie.\\

    Si l'utilisateur avant d'analyser une image souhaite l'éditer, l'application offre la possibilité de redimensionner cette dernière en appuyant dessus. Un menu s'ouvre ensuite pour laisser l'utilisateur éditer l'image. \\

    Ensuite, l'application affiche l'image que nous venons d'envoyer ainsi qu'une liste d'image similaires en dessous. La dernière figure illustre ce cas et est la dernière étape de la démarche de recherche d'image.\\

    L’ensemble des recherches effectuées par l’utilisateur sont ensuite stockées dans une base de données NoSQL nommée ‘Hive’ de type clef-valeurs. Ce stockage en local permet un chargement plus rapide sur l’application et participe a créer une meilleure expérience utilisateur. Grâce à cela, l’application affichera sur la page d’accueil les dernières recherches effectuées par l’utilisateur\footnote{Le schéma de stockage est disponible en annexe.}. La figure à droite illustre parfaitement cet exemple, on retrouve l'écran d'accueil avec les différentes images que l'utilisateur a recherché.

    \chapter{Architecture logicielle}

    Dans ce chapitre, nous allons détailler les interactions entre l'application mobile Patrick et l'API Rest Plankton. La figure Figure~\ref{fig:overview} représente l'architecture que nous utilisons.

    \begin{figure}[ht]
        \centering
        \includegraphics[width=.25\linewidth]{images/overview.png}
        \caption{Résumé de l'architecture}
        \label{fig:overview}
    \end{figure}

    L'application mobile contacte le service qui est hébergé sur Google Kubernetes Engine (\textsc{gke}). L'API dispose donc d'une IP publique, ce qui signifie que l'application mobile peut fonctionner sur un terminal Android ou IOS dès lors que ce dernier a accès à internet.

    \section{Modèle Client/Serveur}

    \paragraph{Application mobile Flutter} L'application mobile est développée en Dart via le framework \href{https://flutter.dev/}{Flutter}\footnote{Flutter est un framework développé par Google et utilisé pour produire des applications Android et IOS.}.

    \paragraph{API Rest Django} Le serveur qui s'occupe de fournir une API Rest tourne sous le framework \href{https://www.djangoproject.com/}{Django}. L'application est livrée via un container Docker et tourne sur un Cluster Kubernetes\footnote{Pour héberger l'application nous utilisons Google Kubernetes Engine.}.

    \section{Exemple de requétage d'une image}

    La figure ci-dessous illustre les échanges entre l'application mobile et le serveur API lors de la recherche d'une image.

    \begin{figure}[ht]
        \centering
        \begin{tikzpicture}[scale=.8]
            \draw[->] (0, 0) -- (0,-6.2);
            \draw[->] (6, 0) -- (6,-6.2);
            \node[draw, fill=white] (client) at (0, 0) {Application mobile};
            \node[draw, fill=white] (server) at (6, 0) {API Rest};

            \draw[->] (0, -.5) -- node[midway, above, sloped] {\scriptsize\texttt{GET /search/}} node[midway, below, sloped] {\tiny\texttt{"image": "data:image/\ldots"}} +(6, -.5);
            \draw[->] (6, -1.5) -- node[midway, above, sloped] {\scriptsize\texttt{JSONResponse}} node[fill=white!50, midway, below, sloped] {\tiny\texttt{"id": 0, "url": "http://\ldots/", "status":200}} +(-6, -.5);

            \draw[->] (0, -2.5) -- node[midway, above, sloped] {\scriptsize\texttt{GET /results/}} +(6, -.5);
            \draw[->] (6, -3.5) -- node[midway, above, sloped] {\scriptsize\texttt{JSONResponse}} node[fill=white!50, midway, below, sloped] {\tiny\texttt{"http://\ldots/", }} +(-6, -.5);


            \draw[->] (0, -4.5) -- node[midway, above, sloped] {\scriptsize\texttt{GET /results/0}} +(6, -.5);
            \draw[->] (6, -5.5) -- node[midway, above, sloped] {\scriptsize\texttt{JSONResponse}} node[fill=white!50, midway, below, sloped] {\tiny\texttt{"http://\ldots/", }} +(-6, -.5);
        \end{tikzpicture}
        \caption{Exemple de requêtage d'image}
    \end{figure}

    Comme nous pouvons le voir, l'application procède en plusieurs étapes. Une première requête consiste à envoyer l'image sous format base 64 afin que le serveur puisse traiter la requête, ce dernier renvoi l'url vers le résultat ce cette requête. La seconde demande de l'application mobile concerne l'url \texttt{/results/} sans passer de clef primaire via l'url. Si aucun id n'est renseigné alors le serveur renvoie cette fois-ci, la liste des recherches que lui a soumis l'application sous forme de tableau. La dernière requête en revanche, concerne toujours l'url \texttt{/results/} mais avec une clef renseignée. Si une recherche avec cet identifiant existe, alors le serveur retourne un tableau contenant des chemins vers les images les plus ressemblantes. L'API Rest dispose également du support des requêtes \textsc{delete} permettant de supprimer une ou toutes les recherches.

    \chapter{Réseau de neurones et traitement de l'image}

    Dans ce chapitre, nous vous détaillerons les spécificités mises en places lors de l'entrainement de notre réseau de neurone ainsi que les choix techniques que nous avons réalisés. \textit{Plus d'informations concernant l'entraînement du réseau de neurones sont aussi disponible dans \href{https://cnn-project.gitlab.io/computer\_vision/html/model.html}{la documentation de l'API}}.


    \paragraph{Sauvegarder les données} Le fichier que nous avons produit à la fin comporte deux tableaux, le premier contient les caractéristiques d'images, et le second contient le chemin de l'image à laquelle la caractéristique correspond. Afin de ne pas stocker les images DeepFashion directement dans notre application Django, nous avons entraîné le réseau de neurones avec des chemins d'image pointant directement sur le dépôt Github DeepFashion. Le code ci-dessous montre la manipulation du chemin de l'image que nous appliquons lors de l'entrainement du réseau de neurones : 

    \begin{minted}{python}

    img_dir = "/content/In-shop-Clothes-From-Deepfashion/Img"  # Emplacement local où se situent les images sur Google Colab
    img_pattern = f"{img_dir}/**/*.jpg"  # Pattern Unix des images
    img_list = glob(img_pattern, recursive=True)  # Renvoie un tableau contenant le chemin de toutes les images
    # ...
    for i, img_path in enumerate(img_list):
        # ... Extraction de la caractéristique
        # Ici on modifie img_name de sorte à garder seulement la fin du chemin vers l'image et de la précéder de l'url du dépot.
        img_name = f"https://raw.githubusercontent.com/aryapei/In-shop-Clothes-From-Deepfashion/master/{'/'.join(img_path.split('/')[3:])}"
        # Ensuite on sauvegarde le nouveau chemin associé à la caractéristique de l'image locale.
        names.append(img_name)
    \end{minted}

    Cette méthode permet ainsi d'entraîner le réseau avec des images ``locales'' sur Google Colab mais égelement de renvoyer sur les images du dépôt Github.


    \section{Extraire les caractéristiques pour les mémoriser}

    \paragraph{Utilisation du r\'eseau de neurones inception resnet v2} Afin de simplifier notre travail, nous avons utilisé un réseau de neurones existant employé dans la classification d'image. En choisissant  cette solution, nous nous assurons de la fiabilité des caractéristiques extraites de nos images en utilisant un modèle approuvé par la communauté.

    \begin{figure}[ht]
        \centering
        \includegraphics[width=.45\linewidth]{images/cnn-comparison.png}
        \caption{Comparaison des performances des différents réseaux de neurones}
    \end{figure}

    Le choix d'inception Resnet v2 n'est pas non plus le fruit du hasard, puisque ce dernier est reconnu pour ses performances en comparaison avec d'autres réseaux de neurones.

    \subparagraph{Normalisation de nos données} Pour pouvoir manipuler plus facilement nos données, nous normalisons la caractéristique de chaque image en la divisant par sa norme matricielle (norme de Frobenius). Soit \mintinline{python}{norm_feat = feat[0] / LA.norm(feat[0])} la caractéristique normalisée que nous conservons. % La norme matricielle (Frobenius) s'exprime sous la forme : $||A||_F = \sqrt{ \sum_{i = 1}^{m}\sum_{j = 1}^{n} | a i j |^2 }$.

    \section{Calculer la distance entre les caractéristiques d'images}

    Une fois la caractéristique de l'image que l'on requête extraite, il faut comparer avec le modèle que nous avons entraîné précédemment afin d'en dénoter les $n$ plus proches caractéristiques. Une fois cette étape faite, il ne reste plus qu'à récupérer les images correspondantes à leur caractéristiques respectives\footnote{Pour rappel, ici les images correspondantes pointent directement sur le dépôt github.}. L'exemple ci-dessous montre l'extraction de la caractéristique d'une image et la récupération des dix images les plus similaires :

    \begin{minted}{python}
    queryVec = model.extract_feat("/content/In-shop-Clothes-From-Deepfashion/Img/WOMEN/Sweaters/id_00000062/01_1_front.jpg")
    scores = np.dot(queryVec, feats.T)
    rank_id = np.argsort(scores)[::-1]
    imlist = (img_names[index].decode('utf-8') for i, index in enumerate(rank_id[0:10]))  # Retour les dix images les plus ressemblantes
    \end{minted}


    \chapter{Conclusion}

    Pour conclure, durant la réalisation de ce projet, nous avons eu l'occasion de découvrir plusieurs concepts et technologies. Ce projet arrive à son terme, et nous allons enfin détailler l'expérience selon les points suivants :

    \section{Développement d'application mobile} La conception d'un application mobile est une première pour nous. Nous avons donc fait face à plusieurs choix déterminant, pour le fonctionnement de notre application : quelle plateforme choisir ? Faut-il utiliser un framework ? Pourquoi ce langage ? \\
    De plus, au-delà  des choix techniques, il est aussi important de réfléchir à l'ergonomie de notre application. En effet, comment arranger notre application ? Quelles étapes nécessaires à l'envoi d'une image ? Tout ce procédé de réflexion amène donc à une certaine expérience en termes de conception et prend une place importante, sachant qu'il s'agit de l'interface visible avec l'utilisateur. 

    \section{Découverte des réseaux de neurones à convolutions} Au cours de cet exercice, nous avons eu l'occasion de mettre en pratique, mais avant tout, de renforcer nos connaissances à propos des réseaux de neurones.  Cela nous a permis dans un premier temps, de mieux comprendre les réseaux de neurones en terme d'architecture, d'appréhender les fonctions des layers d'un réseau de neurones mais aussi et surtout de savoir mettre en place un réseau de neurones \textit{Content Based Image Retrieval} (\textsc{CBIR}).

    \section{Conception et déploiement d'un projet} Ce projet a été pour nous, l'occasion d'expérimenter la conception au déploiement d'un service de A à Z. Durant cet exercice, nous avons avons eu la possibilité de tester la mise en production de containers Docker sur un cluster Kubernetes. L'introduction au Dart et à son framework Flutter qui nous était jusqu'alors inconnu, fût alors un exercice bénéfique, apprenant ainsi de nouveau paradigmes tel que la conception d'une application sous forme de widgets.

    \part{Annexes}

    \chapter{Fichiers de permissions}
    Les librairies utilisés dans le développement de l’application : (extraite ici du fichier de configuration global du projet ‘pubspec.yaml’. 
    \begin{minted}{yaml}
    # Permet de récupérer les chemins de stockage de la plateforme (iOS/Android) 
    path: ^1.6.4 
    path_provider: ^1.6.5 
    
    # Requête http et stockage de l'information 
    dio: ^3.0.9 
    hive: ^1.4.1+1 
    hive_flutter: ^0.3.0+2 
    
    # Utilisation de la caméra 
    camera: ^0.5.7+3 
    image_picker: ^0.6.2+3 
    image_cropper: ^1.2.1 
    
    # Ajout de fonctionalités graphiques 
    flutter_launcher_icons: ^0.7.3 
    splashscreen: ^1.2.0 
    flutter_gradient_colors: ^1.0.0 
    gradient_text: ^1.0.2 
    \end{minted}

    \chapter{Le schéma de stockage}
    Exemple de persistance des données dans la base NoSql ‘Hive’ pour deux images sauvegardées en local 
    \begin{minted}{json}
    { 
       "0":{ 
          "date":"timestamp0", 
          "remoteUrl":"http://35.228.249.118:8000/results/1", 
          "base64Image":"/9j/4AAQSkZ...4QC8RX", 
          "correspondingPictures":[ 
             "imageUrl1", 
             "imageUrl2", 
             "..." 
          ] 
       }, 
       "1":{ 
          "date":"timestamp1", 
          "remoteUrl":"http://35.228.249.118:8000/results/2", 
          "base64Image":"/9j/yMjGRAQA...4QC8RX", 
          "correspondingPictures":[ 
             "imageUrl1", 
             "imageUrl2", 
             "..." 
          ] 
       } 
    } 
    \end{minted}



\end{document}
