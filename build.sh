#/usr/bin/bash

if [[ ! -d out ]]; then
	mkdir out
fi
xelatex --shell-escape --output-dir=out report.tex
cd out && makeglossaries -q report && cd ..
xelatex --shell-escape --output-dir=out report.tex
cp out/report.pdf ./report.pdf